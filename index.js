const { ipcRenderer, remote } = require('electron');
        const fs = require('fs');
        const mainProcess = remote.require('./main');

        var currentLibraryPath = "";
        var currentWindow = "";

        var musicQueue = [];
        var popup;

        // Main hierarchy declaration
        var hierarchy = {
            "container": document.getElementById("container"),
            "screens": {
                "search": {

                },
                "library": {

                },
                "player": {

                },
                "queue": {

                },
                "settings": {

                }
            },
            "buttons": {}
        };

        mainSetup();

        // Main hierarchy setup
        function mainSetup() {
            hierarchy["screens"]["mDiv"] = document.createElement("div");
            hierarchy["buttons"]["mDiv"] = document.createElement("div");

            hierarchy["screens"]["mDiv"].className = "screensDiv";
            hierarchy["buttons"]["mDiv"].className = "buttonsDiv";

            hierarchy["container"].appendChild(hierarchy["screens"]["mDiv"]);
            hierarchy["container"].appendChild(hierarchy["buttons"]["mDiv"]);

            ipcRenderer.send("html:main");

            footerSetup();
            searchWinSetup();
            libWinSetup();
            playerWinSetup();
            setWinSetup();
            listenerSetup();
            popupSetup();
        }

        // Footer buttons setup + add to hierarchy
        function footerSetup() {
            for(var i in Object.keys(hierarchy["screens"])) {
                if(Object.keys(hierarchy["screens"])[i] == "mDiv") continue;
                // Setting up main window holders
                hierarchy["screens"][Object.keys(hierarchy["screens"])[i]]["mDiv"] = document.createElement("div");
                hierarchy["screens"][Object.keys(hierarchy["screens"])[i]]["mDiv"].className = "mainWindowDiv";

                // Setting up main buttons
                hierarchy["buttons"][`${Object.keys(hierarchy["screens"])[i]}Btn`] = document.createElement("button");
                hierarchy["buttons"][`${Object.keys(hierarchy["screens"])[i]}Btn`].appendChild(document.createTextNode(Object.keys(hierarchy["screens"])[i].charAt(0).toUpperCase() + Object.keys(hierarchy["screens"])[i].slice(1)));
                hierarchy["buttons"][`${Object.keys(hierarchy["screens"])[i]}Btn`].className = "mainButton";
                hierarchy["buttons"]["mDiv"].appendChild(hierarchy["buttons"][`${Object.keys(hierarchy["screens"])[i]}Btn`]);

                hierarchy["buttons"][`${Object.keys(hierarchy["screens"])[i]}Btn`].addEventListener("click", (e) => {
                    loadWindow(e.path[0].innerHTML.toLowerCase());
                    currentWindow = e.path[0].innerHTML.toLowerCase();
                });
            }

        }

        // Search window setup
        function searchWinSetup() {
            hierarchy["screens"]["search"]["searchBar"] = document.createElement("input");
            hierarchy["screens"]["search"]["resultsDiv"] = document.createElement("div");

            hierarchy["screens"]["search"]["searchBar"].className = "searchBar";
            hierarchy["screens"]["search"]["resultsDiv"].className = "resultsDiv";

            hierarchy["screens"]["search"]["searchBar"].placeholder = "Search YouTube...";

            hierarchy["screens"]["search"]["mDiv"].appendChild(hierarchy["screens"]["search"]["searchBar"]);
            hierarchy["screens"]["search"]["mDiv"].appendChild(hierarchy["screens"]["search"]["resultsDiv"]);

            hierarchy["screens"]["search"]["searchBar"].addEventListener("keypress", (e) => {
                if(e.keyCode != 13) return;
                while (hierarchy["screens"]["search"]["resultsDiv"].firstChild) hierarchy["screens"]["search"]["resultsDiv"].removeChild(hierarchy["screens"]["search"]["resultsDiv"].firstChild);
                if(hierarchy["screens"]["search"]["searchBar"].value == "") return;
                hierarchy["screens"]["search"]["searchBar"].setSelectionRange(0, hierarchy["screens"]["search"]["searchBar"].value.length);
                ipcRenderer.send("request:ytSearch", hierarchy["screens"]["search"]["searchBar"].value);
            });
        }

        // Library window setup
        function libWinSetup() {
            hierarchy["screens"]["library"]["searchBar"] = document.createElement("input");
            hierarchy["screens"]["library"]["resultsDiv"] = document.createElement("div");

            hierarchy["screens"]["library"]["searchBar"].className = "searchBar";
            hierarchy["screens"]["library"]["resultsDiv"].className = "resultsDiv";

            hierarchy["screens"]["library"]["searchBar"].placeholder = "Search music library...";

            hierarchy["screens"]["library"]["mDiv"].appendChild(hierarchy["screens"]["library"]["searchBar"]);
            hierarchy["screens"]["library"]["mDiv"].appendChild(hierarchy["screens"]["library"]["resultsDiv"]);

            hierarchy["screens"]["library"]["searchBar"].addEventListener("keypress", (e) => {
                if(e.keyCode != 13) return;
                e.path[0].setSelectionRange(0, e.path[0].value.length);
                ipcRenderer.send("request:musicLibrary", currentLibraryPath, e.path[0].value);
            });


        }

        // Player window setup
        function playerWinSetup() {
            hierarchy["screens"]["player"]["srcDiv"] = document.createElement("div");
            //hierarchy["screens"]["player"]["ctrlDiv"] = document.createElement("div");

            hierarchy["screens"]["player"]["srcDiv"].className = "mainWindowDiv";
            //hierarchy["screens"]["player"]["ctrlDiv"].className = "";

            hierarchy["screens"]["player"]["mDiv"].appendChild(hierarchy["screens"]["player"]["srcDiv"]);
            //hierarchy["screens"]["player"]["mDiv"].appendChild(hierarchy["screens"]["player"]["ctrlDiv"]);

        }

        // Settings window setup
        function setWinSetup() {
            hierarchy["screens"]["settings"]["mDiv"].appendChild(buildSettingsItem("Music Library Folder", () => {
                var mli = document.createElement("input");
                var selected = false;
                mli.className = "pathInput";

                mli.addEventListener("click", (e) => {
                    if(selected) return;
                    selected = true;
                    var dir = mainProcess.selectDirectory();
                    if(typeof dir == "undefined") return;
                    if(!fs.existsSync(dir)) return;
                    mli.value = dir.split("").splice(require("os").homedir().length, dir.length).join("");
                    mli.setSelectionRange(0, mli.value.length);
                    currentLibraryPath = `${require("os").homedir()}/${mli.value}`;
                });

                mli.addEventListener("keypress", (e) => {
                    if(e.keyCode != 13) return;
                    if(!fs.existsSync(`${require("os").homedir()}/${mli.value}`)) return;
                    mli.setSelectionRange(0, mli.value.length);
                    currentLibraryPath = `${require("os").homedir()}/${mli.value}`;
                });

                mli.addEventListener("focusout", (e) => {
                    selected = false;
                });

                return mli;
            }));
        }

        // Event listeners
        function listenerSetup() {


            ipcRenderer.on("drop:musicLibrary", (e, library, filter) => {
                while(hierarchy["screens"]["library"]["resultsDiv"].firstChild) hierarchy["screens"]["library"]["resultsDiv"].removeChild(hierarchy["screens"]["library"]["resultsDiv"].firstChild);

                var jsonFile = JSON.parse(fs.readFileSync(`${currentLibraryPath}/videos.json`));
                for(var i in library) {

                    if(library[i].ext != ".mp4") continue;
                    if(filter != "") {
                        if(!jsonFile[library[i].name].toLowerCase().includes(filter.toLowerCase())) continue;
                    }

                    hierarchy["screens"]["library"]["resultsDiv"].appendChild(buildLibraryItem(library[i].name, jsonFile[library[i].name]));
                }
            });

            ipcRenderer.on("drop:ytResults", (e, results) => {
                for(var i in results) {
                    hierarchy["screens"]["search"]["resultsDiv"].appendChild(buildSearchResult(results[i]["title"], results[i]["id"]));
                }
            });

            ipcRenderer.on("window:switch", (e, w) => {
                loadWindow(w);
            });

            ipcRenderer.on("popup", (e) => {
                popup.style.display = "block";
            });


        }

        function popupSetup() {
            popup = document.createElement("div");
            popup.className = "popup";
            var content = document.createElement("div");

            var input = document.createElement("input");
            input.className = "popup-input";
            content.appendChild(document.createTextNode("Enter link and press enter"));
            content.appendChild(input);

            content.className = "popup-content";
            popup.appendChild(content);
            popup.onclick = function(event) {
                if (event.target == popup) {
                    popup.style.display = "none";
                }
            }
            hierarchy["container"].appendChild(popup);
        }

        // Misc functions
        function clearWindows() {
            for (var i in hierarchy["screens"]["mDiv"].childNodes) {
                if(hierarchy["screens"]["mDiv"].childNodes.length != 0) hierarchy["screens"]["mDiv"].removeChild(hierarchy["screens"]["mDiv"].childNodes[i]);
            }

            for(var j in hierarchy["buttons"]) {
                if(Object.keys(hierarchy["buttons"])[j] != "mDiv") {
                    if(hierarchy["buttons"][j].hasAttribute("disabled")) hierarchy["buttons"][j].removeAttribute("disabled");
                }
            }
        }

        function buildSearchResult(t, id) {
            var mDiv = document.createElement("div");
            var title = document.createTextNode(t);
            var dlBtn = document.createElement("button");
            var dlTxt = document.createTextNode("Download");

            mDiv.className = "tileDiv";
            dlBtn.className = "tileButton";

            dlBtn.addEventListener("click", (e) => {
                ipcRenderer.send("request:download", id, t, currentLibraryPath);
            });

            mDiv.appendChild(title);
            mDiv.appendChild(dlBtn);
            dlBtn.appendChild(dlTxt);

            return mDiv;
        }

        function buildQueueItem(t, i) {
            var mDiv = document.createElement("div");
            var title = document.createTextNode(t);
            var delBtn = document.createElement("button");

            mDiv.className = "tileDiv";
            delBtn.className = "tileButton";

            delBtn.addEventListener("click", (e) => {
                musicQueue.splice(i, 1);
                loadWindow("queue");
            });

            mDiv.appendChild(title);
            mDiv.appendChild(delBtn);
            delBtn.appendChild(document.createTextNode("Remove from queue"));

            return mDiv;
        }

        function buildLibraryItem(id, title) {
            var mDiv = document.createElement("div");
            var playBtn = document.createElement("button");
            var playTxt = typeof hierarchy["screens"]["player"]["player"] == "undefined" ? document.createTextNode("Play") : document.createTextNode("Add to queue");

            mDiv.className = "tileDiv";
            playBtn.className = "tileButton";

            playBtn.addEventListener("click", (e) => {
                next = document.createElement("video");
                next.setAttribute("controls", "controls");

                var source = document.createElement("source");
                source.id = id;
                source.src = `${currentLibraryPath}/${id}.mp4`;
                source.type = "video/mp4";
                next.appendChild(source);

                next.onended = (ev) => {
                    hierarchy["screens"]["player"]["player"].pause();
                    hierarchy["screens"]["player"]["player"].parentNode.removeChild(hierarchy["screens"]["player"]["player"]);

                    // Test if there is another song in queue, then:
                    if(musicQueue.length > 0) {
                        hierarchy["screens"]["player"]["player"] = musicQueue[0];

                        hierarchy["screens"]["player"]["srcDiv"].appendChild(hierarchy["screens"]["player"]["player"]);
                        hierarchy["screens"]["player"]["player"].play();
                        musicQueue.shift();
                    } else {
                        hierarchy["screens"]["player"]["player"] = undefined;
                    }

                    if(currentWindow == "queue") loadWindow("queue");
                };


                musicQueue.push(next);

                // If there is nothing playing, then:
                if(typeof hierarchy["screens"]["player"]["player"] == "undefined") {

                    // Set the player to the one next in queue
                    hierarchy["screens"]["player"]["player"] = musicQueue[0];

                    // Append the player containing the new song to the player window
                    hierarchy["screens"]["player"]["srcDiv"].appendChild(hierarchy["screens"]["player"]["player"]);

                    // Play the song
                    hierarchy["screens"]["player"]["player"].play();

                    // Remove the first entry in the musicQueue array, in this case It'll be the song that was just played
                    musicQueue.shift();

                    // Refresh the music library, this is done so that text on the buttons say "Add to queue" instead of "Play"
                    ipcRenderer.send("request:musicLibrary", currentLibraryPath, "");
                }
            });

            mDiv.appendChild(document.createTextNode(title));
            mDiv.appendChild(playBtn);
            playBtn.appendChild(playTxt);

            return mDiv;
        }

        function buildSettingsItem(t, input) {
            var mDiv = document.createElement("div");
            var title = document.createTextNode(t);

            mDiv.className = "settingTile";

            mDiv.appendChild(title);
            mDiv.appendChild(input());

            return mDiv;
        }

        function loadWindow(w) {
            if(w != "player") if(typeof hierarchy["screens"]["player"]["player"] != "undefined") var paused = hierarchy["screens"]["player"]["player"].paused;
            clearWindows();
            hierarchy["buttons"][`${w}Btn`].setAttribute("disabled", "true");
            hierarchy["screens"]["mDiv"].appendChild(hierarchy["screens"][w]["mDiv"]);
            if(w != "player") if(typeof hierarchy["screens"]["player"]["player"] != "undefined") if(!paused) hierarchy["screens"]["player"]["player"].play();
            if(w == "library") {
                if(!fs.existsSync(currentLibraryPath)) return;
                ipcRenderer.send("request:musicLibrary", currentLibraryPath, "");
            }

            if(w == "queue") {
                if(!fs.existsSync(`${currentLibraryPath}/videos.json`)) return;
                var jsonFile = JSON.parse(fs.readFileSync(`${currentLibraryPath}/videos.json`));
                while(hierarchy["screens"]["queue"]["mDiv"].firstChild) hierarchy["screens"]["queue"]["mDiv"].removeChild(hierarchy["screens"]["queue"]["mDiv"].firstChild);
                if(typeof hierarchy["screens"]["player"]["player"] != "undefined") {
                    var tileDiv = document.createElement("div");
                    tileDiv.className = "tileDiv";
                    tileDiv.appendChild(document.createTextNode(jsonFile[hierarchy["screens"]["player"]["player"].firstChild.id]));
                    hierarchy["screens"]["queue"]["mDiv"].appendChild(tileDiv);
                    for(var i in musicQueue) {
                        hierarchy["screens"]["queue"]["mDiv"].appendChild(buildQueueItem(jsonFile[musicQueue[i].firstChild.id], i));
                    }
                }
            }
        }
const { BrowserWindow, app, ipcMain, Menu, dialog} = require('electron');
const api = require('simple-youtube-api');
const fs = require('fs');
const path = require('path');
const ytdl = require('ytdl-core');

const youtube = new api("AIzaSyDSnQZKW9s0QQJj767K3X4YJpf1NvyISlg");

app.on("ready", () => {
    mainWindow = new BrowserWindow({
        width: 600,
        height: 400
    });

    mainWindow.loadURL(`file://${__dirname}/main.html`);
    Menu.setApplicationMenu(Menu.buildFromTemplate(getMenuTemplate(process.platform)));
});

app.on("window-all-closed", () => {
    app.quit();
});

ipcMain.on("request:musicLibrary", (e, path, filter) => {
    e.sender.send("drop:musicLibrary", getFiles(path), filter);
});

ipcMain.on("request:download", (e, id, name, path) => {
    console.log(`Downloading ${id}`);
    ytdl(`http://www.youtube.com/watch?v=${id}`, { filter: (format) => format.container === 'mp4' }).pipe(fs.createWriteStream(`${path}/${id}.mp4`));
    if(!fs.existsSync(`${path}/videos.json`)) fs.writeFileSync(`${path}/videos.json`, JSON.stringify({}));
    var jsonFile = JSON.parse(fs.readFileSync(`${path}/videos.json`));
    jsonFile[id] = parseHtmlEntities(name);
    fs.writeFileSync(`${path}/videos.json`, JSON.stringify(jsonFile));
});

ipcMain.on("request:ytSearch", (e, query) => {
    searchYouTube(query, e.sender);
});

function getFiles(p) {
    var files = [];

    fs.readdirSync(p).forEach(filename => {
        const name = path.parse(filename).name;
        const ext = path.parse(filename).ext;
        const filepath = path.resolve(p, filename);
        const stat = fs.statSync(filepath);
        const isFile = stat.isFile();

        if (isFile) files.push({ filepath, name, ext, stat });
    });

    files.sort((a, b) => {
        return a.name.localeCompare(b.name, undefined, { numeric: true, sensitivity: 'base' });
    });

    return files;
}

async function searchYouTube(query, sender) {
    console.log(query);
    var videos = [];
    await youtube.searchVideos(query, 10).then((results) => {
        for(var i in results) {
            videos.push({
                "title": results[i].raw.snippet.title,
                "id": results[i].raw.id.videoId
            });
        }
        sender.send("drop:ytResults", videos);
    });
}

function parseHtmlEntities(str) {
    return str.replace(/&#([0-9]{1,3});/gi, (match, numStr) => {
        var num = parseInt(numStr, 10); // read num as normal number
        return String.fromCharCode(num);
    });
}

 function getMenuTemplate(p) {
     switch (p) {
         case "darwin":
            return [
                {
                    label: "macDefault",
                    submenu: [
                        {
                            label: "About MusicApp"
                        },
                        {
                            label: "Version 1.0.0",
                            enabled: false
                        },
                        {
                            type: "separator"
                        },
                        {
                            label: "Preferences",
                            accelerator: "Cmd+,",
                            click() {
                                mainWindow.send("window:switch", "settings");
                            }
                        }
                    ]
                },
                {
                    label: "More Functions",
                    submenu: [
                        {
                            label: "Add Song From Link",
                            accelerator: "Cmd+L",
                            click() {
                                mainWindow.send("popup");
                            }
                        },
                        {
                            label: "Reload",
                            accelerator: "Cmd+R",
                            click() {
                                mainWindow.reload();
                            }
                        },
                        {
                            label: "Dev Tools",
                            click() {
                                mainWindow.toggleDevTools();
                            }
                        }
                    ]
                }
            ];
             break;

         case "win32":
            return [
                {
                    label: "More Functions",
                    submenu: [
                        {
                            label: "Add Song From Link",
                            accelerator: "Ctrl+L",
                            click() {
                                //popup
                            }
                        }
                    ]
                }
            ];
             break;
     }
 }

 function selectDirectory() {
     var dir = dialog.showOpenDialog(mainWindow, {
         properties: ['openDirectory']
     });

     return typeof dir == "undefined" ? dir : dir[0];
 }

 exports.selectDirectory = selectDirectory;
